**A project to represent the total home and away goals scored by season in English football history from 1888 - 2016**

![Apollo 11](https://bitbucket.org/ruhksn/english-football-historic/raw/ff17e24bfc46e858974d3c93ce3dbb371697592a/home-away-goals.png)
